# Project: polaris-studio

by: Iceyisaak

### This project features:

- Digital Agency Business Website
- Fixed Collapsible Navbar (right)
- Card Elements
- Css Grid Gallery

### Technologies used
- Pure HTML/CSS (SASS)
- FontAwesome
- GoogleFonts

## Installation
1. `npm install` to create node_modules
2. `npm run start` to run the project on live-server
3. ENJOY!

